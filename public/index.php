<?php

header('Content-Type: text/html; charset=UTF-8');
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  $messages = array();
    if (!empty($_COOKIE['exist'])) {
      $messages['save'] = '<div>Email занят</div>'; }
  if (!empty($_COOKIE['save'])) {
    setcookie('save', '', 100000);
    setcookie('exist', '', 100000);
    setcookie('login', '', 100000);
    setcookie('password', '', 100000);
    $messages['save'] = 'Спасибо, результаты сохранены. ';
  if (!empty($_COOKIE['password'])) {
      $messages[] = sprintf('Вы можете войти с логином <strong>%s</strong>
        и паролем <strong>%s</strong> для изменения данных.',
        strip_tags($_COOKIE['login']),
        strip_tags($_COOKIE['password']));
    }
  }
  $errors = array();
  $errors['fio'] = !empty($_COOKIE['fio_error']);
  $errors['email'] = !empty($_COOKIE['email_error']);
  $errors['pol'] = !empty($_COOKIE['pol_error']);
  $errors['date'] = !empty($_COOKIE['date_error']);
  $errors['limbs'] = !empty($_COOKIE['limbs_error']);
  $errors['abilities'] = !empty($_COOKIE['abilities_error']);
  $errors['biography'] = !empty($_COOKIE['biography_error']);
  $errors['check'] = !empty($_COOKIE['check_error']);
  
  if (!empty($errors['fio'])) {
    setcookie('fio_error', '', 100000);
    $messages[] = '<div class="error">Заполните имя.</div>';
  }
  if(!empty($errors['email'])) {
  setcookie('email_error', '', 100000);
  $messages[] = '<div class="error">Заполните почту.</div>';
  }
  if (!empty($errors['pol'])) {
    setcookie('pol_error', '', 100000);
    $messages[] = '<div class="error">Заполните пол.</div>';
  }
  if(!empty($errors['date'])) {
  setcookie('date_error', '', 100000);
  $messages[] = '<div class="error">Заполните дату.</div>';
  }
  if(!empty($errors['limbs'])) {
  setcookie('limbs_error', '', 100000);
  $messages[] = '<div class="error">Выберете количество.</div>';
  }
  if(!empty($errors['abilities'])) {
  setcookie('abilities_error', '', 100000);
  $messages[] = '<div class="error">Выберете способность.</div>';
  }
  if(!empty($errors['biography'])) {
  setcookie('biography_error', '', 100000);
  $messages[] = '<div class="error">Заполните Биографию.</div>';
  }
  if (!empty($errors['check'])) {
    setcookie('check_error', '', 100000);
    $messages[] = '<div class="error">Поставте галочку.</div>';
}
  $values = array();
  $values['fio'] = empty($_COOKIE['fio_value']) ? '' : strip_tags($_COOKIE['fio_value']);
  $values['email'] = empty($_COOKIE['email_value']) ? '' : strip_tags($_COOKIE['email_value']);
  $values['date'] = empty($_COOKIE['date_value']) ? '' : strip_tags($_COOKIE['date_value']);
  $values['pol'] = empty($_COOKIE['pol_value']) ? '' : strip_tags($_COOKIE['pol_value']);
  $values['limbs'] = empty($_COOKIE['limbs_value']) ? '' : strip_tags($_COOKIE['limbs_value']);
  $values['abilities'] = empty($_COOKIE['abilities_value']) ? '' : strip_tags($_COOKIE['abilities_value']);
  $values['biography'] = empty($_COOKIE['biography_value']) ? '' : strip_tags($_COOKIE['biography_value']);
  if (!empty($_COOKIE[session_name()]) &&
      session_start() && !empty($_SESSION['login'])) {
      
  $user = 'u15824';
  $pass = '4568020';
  $db = new PDO('mysql:host=localhost;dbname=u15824', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
  
   try {

        $stmt = $db->prepare("SELECT name FROM application1 WHERE email = ?");
        $stmt -> execute([$_SESSION['login']]);
        $values['fio'] = $stmt->fetchColumn();
        
        $values['email'] = $_SESSION['login'];
        
        $stmt = $db->prepare("SELECT year FROM application1 WHERE email = ?");
        $stmt->execute([$_SESSION['login']]);
        $values['date'] = $stmt->fetchColumn();
        
        $stmt = $db->prepare("SELECT pol FROM application1 WHERE email = ?");
        $stmt->execute([$_SESSION['login']]);
        $values['pol'] = $stmt->fetchColumn();
        
        $stmt = $db->prepare("SELECT limbs FROM application1 WHERE email = ?");
        $stmt->execute([$_SESSION['login']]);
        $values['limbs'] = $stmt->fetchColumn();
        
        $stmt = $db->prepare("SELECT abilities FROM application1 WHERE email = ?");
        $stmt->execute([$_SESSION['login']]);
        $values['abilities'] = $stmt->fetchColumn();
        
        $stmt = $db->prepare("SELECT biography FROM application1 WHERE email = ?");
        $stmt->execute([$_SESSION['login']]);
        $values['biography'] = $stmt->fetchColumn();
        

        
    }
    catch(PDOException $e){
        print('Error : ' . $e->getMessage());
        exit();
    } 

    $messages[] ='Вход с логином';
    $messages[] = $_SESSION['login'];
  }
  
  include('form.php');
}
  
  else {
  $errors = FALSE;
  if (empty($_POST['fio'])) {
    setcookie('fio_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('fio_value', $_POST['fio'], time() + 30 * 24 * 60 * 60);
  }
  
  if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
  setcookie('email_error', '1', time() + 24 * 60 * 60);
  $errors = TRUE;
}
else {
  setcookie('email_value', $_POST['email'], time() + 30 * 24 * 60 * 60);
}
if (empty($_POST['date'])) {
  setcookie('date_error', '1', time() + 24 * 60 * 60);
  $errors = TRUE;
}
else {
  setcookie('date_value', $_POST['date'], time() + 30 * 24 * 60 * 60);
}
if (empty($_POST['pol'])) {
  setcookie('pol_error', '1', time() + 24 * 60 * 60);
  $errors = TRUE;
}
else {
  setcookie('pol_value', $_POST['pol'], time() + 30 * 24 * 60 * 60);
}
if (empty($_POST['limbs'])) {
  setcookie('limbs_error', '1', time() + 24 * 60 * 60);
  $errors = TRUE;
}
else {
  setcookie('limbs_value', $_POST['limbs'], time() + 30 * 24 * 60 * 60);
}
if (empty($_POST['biography'])) {
  setcookie('biography_error', '1', time() + 24 * 60 * 60);
  $errors = TRUE;
}
else {
  setcookie('biography_value', $_POST['biography'], time() + 30 * 24 * 60 * 60);
}

if (empty($_POST['abilities'])) {
  setcookie('abilities_error', '1', time() + 24 * 60 * 60);
  $errors = TRUE;
}
else {
  setcookie('abilities_value', $_POST['abilities'], time() + 30 * 24 * 60 * 60);
}
if (empty($_POST['check'])) {

  setcookie('check_error', '1', time() + 24 * 60 * 60);
  $errors = TRUE;
}
else {

  setcookie('check_value', $_POST['check'], time() + 30 * 24 * 60 * 60);
} 
if ($errors) {
  header('Location: index.php');
  exit();
  }
else {
  setcookie('fio_error', '', 100000);
  setcookie('email_error', '', 100000);
  setcookie('date_error', '', 100000);
  setcookie('pol_error', '', 100000);
  setcookie('limbs_error', '', 100000);
  setcookie('biography_error', '', 100000);
  setcookie('check_error', '', 100000);
  setcookie('abilities_error', '', 100000);
} 
  switch($_POST['pol']) {
    case 'm': {
        $pol='m';
        break;
    }
    case 'f':{
        $pol='f';
        break;
    }
};

  switch($_POST['limbs']) {
    case '1': {
        $limbs='1';
        break;
    }
    case '2':{
        $limbs='2';
        break;
    }
    case '3':{
        $limbs='3';
        break;
    }
    case '4':{
        $limbs='4';
        break;
    }
};
$abilities = serialize($_POST['abilities']);
  if (!empty($_COOKIE[session_name()]) &&
      session_start() && !empty($_SESSION['login'])) {
  $user = 'u15824';
  $pass = '4568020';
  $db = new PDO('mysql:host=localhost;dbname=u15824', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
  $messages[] = "email изменен не будет";
  try {
        $stmt = $db->prepare("UPDATE application1 SET name = ?, year = ?, pol = ?, limbs = ?, abilities = ?, biography = ? WHERE email = ?");
        $stmt -> execute(array($_POST['fio'],$_POST['date'],$pol,$limbs,$abilities,$_POST['biography'],$_SESSION['login']));
    }
    catch(PDOException $e){
        print('Error : ' . $e->getMessage());
        exit();
    }
    setcookie('save', '1');
    }
    else {
      $user = 'u15824';
      $pass = '4568020';
      $db = new PDO('mysql:host=localhost;dbname=u15824', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
      try {
      $stmt = $db->prepare("SELECT email FROM application1 WHERE email = ?");
      $stmt -> execute([$_POST['email']]);
      $log = $stmt->fetchColumn();
      }
      catch(PDOException $e){
          print('Error : ' . $e->getMessage());
          exit();
      }
      if ($log != NULL){
          setcookie('exit', '1');
      }
      else {
    $login = $_POST['email'];
    $password = substr(rand(10000,100000), 2);
    setcookie('login', $login);
    setcookie('password', $password);
    $password1 = md5($password);
     try {
        $stmt = $db->prepare("INSERT INTO application1 SET name = ?, email = ?, year = ?, pol = ?, limbs = ?, abilities = ?, biography = ?, pass = ?");
        $stmt -> execute(array($_POST['fio'],$_POST['email'],$_POST['date'],$pol,$limbs,$abilities,$_POST['biography'], $password1));
    }
    catch(PDOException $e){
        print('Error : ' . $e->getMessage());
        exit();
    }
    setcookie('save', '1');
    }
  }
    $new_url = 'index.php';
  header('Location: '.$new_url);
}


?>    
